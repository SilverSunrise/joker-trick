package com.jok.kirt

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_policy.*


class PolicyActivity : AppCompatActivity() {
    @SuppressLint("SetJavaScriptEnabled")
    private fun WebView.setDefaultJSSettings() {
        settings.apply {
            loadUrl("https://joker-trick.flycricket.io/privacy.html")
            javaScriptCanOpenWindowsAutomatically = true
            setSupportMultipleWindows(true)
            javaScriptEnabled = true
            domStorageEnabled = true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_policy)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        iv_backToMenu.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        wv_policy.webViewClient = WebViewClient()
        wv_policy.setDefaultJSSettings()
    }
}