package com.jok.kirt

import android.app.Activity
import android.content.SharedPreferences
import androidx.multidex.MultiDexApplication
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.facebook.FacebookSdk
import com.facebook.LoggingBehavior
import com.onesignal.OneSignal
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class JokerTrick : MultiDexApplication() {
    companion object {
        const val adev = "WxECMdHugQbSkJFQyGiw6V"
        const val isdk = "b8f5d9cc-1d21-4433-a776-2a64a5604d61"
        const val pref = "prefs"
    }

    lateinit var trickPrefs: SharedPreferences

    var mserv: MainService? = null
    var trickserv: LoggingService? = null

    fun iFly(conversionListener: AppsFlyerConversionListener, activity: Activity) {
        AppsFlyerLib.getInstance().enableFacebookDeferredApplinks(false)
        AppsFlyerLib.getInstance().init(adev, conversionListener, activity)
        AppsFlyerLib.getInstance().start(activity)
    }

    override fun onCreate() {
        super.onCreate()

        trickPrefs = getSharedPreferences(pref, MODE_PRIVATE)
        iAll()
    }

    private fun iAll() {
        iSign()
        iAps()
        iFace()
        iYan()
    }

    private fun iYan() {
        val settings = YandexMetricaConfig
            .newConfigBuilder(isdk)
            .build()
        YandexMetrica.activate(this, settings)
        YandexMetrica.enableActivityAutoTracking(this)
    }

    private fun iAps() {
        trickserv = Retrofit.Builder()
            .baseUrl("https://tbraza.club/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(LoggingService::class.java)

        mserv = Retrofit.Builder()
            .baseUrl("https://install-tracker.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(MainService::class.java)
    }


    private fun iSign() {
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }

    private fun iFace() {
        FacebookSdk.setAutoInitEnabled(true)
        FacebookSdk.setAdvertiserIDCollectionEnabled(true)
        FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS)
        FacebookSdk.sdkInitialize(this)
        //todo
//        AppEventsLogger.activateApp(this)
//        var facebookLogger = AppEventsLogger.newLogger(this)
//        facebookLogger.logEvent(
//                AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, Bundle().apply {
//            putString(AppEventsConstants.EVENT_PARAM_CURRENCY, "USD")
//            putInt(AppEventsConstants.EVENT_PARAM_VALUE_TO_SUM, 1)
//        }
//        )
//        facebookLogger.logPurchase(
//                BigDecimal.valueOf(1),
//                Currency.getInstance("USD")
//        )
    }

}
