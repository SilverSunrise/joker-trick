package com.jok.kirt

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Message
import android.view.View
import android.view.WindowManager
import android.webkit.ValueCallback
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity


class TrickActivity : AppCompatActivity() {

    private fun superChooser() {
        val joker = Intent(Intent.ACTION_GET_CONTENT)
        joker.addCategory(Intent.CATEGORY_OPENABLE)
        joker.type = "image/*"
        startActivityForResult(Intent.createChooser(joker, "Image Chooser"), jokerNumber)
    }

    override fun onResume() {
        super.onResume()
        val decorView = window.decorView
        decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0) {
                decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setFlags( WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)
//        window.setFlags(
//                WindowManager.LayoutParams.FLAG_FULLSCREEN or WindowManager.LayoutParams.FLAG_SECURE,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN or WindowManager.LayoutParams.FLAG_SECURE
//        )

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            window.decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        }

        setContentView(R.layout.activity_trick)

        trickView = findViewById(R.id.trickView)
        trickCont = findViewById(R.id.container)
        trickPro = findViewById(R.id.trickPro)

        trickView.trickProWeb()
        trickView.setDefaultJSSettings()

        trakJokLink()?.let {
            trickView.loadUrl(it)
        }
    }



    private lateinit var uriCallback: ValueCallback<Array<Uri?>>
    private lateinit var trickView: WebView
    private lateinit var trickCont: FrameLayout
    private lateinit var trickPro: ProgressBar
    private var trickWeb: WebView? = null
    private val jokerNumber = 100


    override fun onBackPressed() {

        if (trickWeb?.visibility == View.VISIBLE) {
            if (trickWeb?.canGoBack() == true) {
                trickWeb?.goBack()
            } else {
                trickWeb?.destroy()
                trickCont.removeView(trickWeb)
                trickView.visibility = View.VISIBLE
            }
        } else if (trickView.canGoBack()) {
            trickView.goBack()
        } else {
            super.onBackPressed()
        }
    }


    @SuppressLint("SetJavaScriptEnabled")
    fun WebView.setDefaultJSSettings() {
        settings.apply {
            javaScriptCanOpenWindowsAutomatically = true
            setSupportMultipleWindows(true)
            javaScriptEnabled = true
            domStorageEnabled = true
        }
    }


    override fun onActivityResult(
            requestCode: Int,
            resultCode: Int,
            data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == jokerNumber) {
            val finalNum =
                    if (data == null || resultCode != Activity.RESULT_OK) null else data.data

            if (finalNum != null) {
                uriCallback.onReceiveValue(arrayOf(finalNum))
            }
        }
    }



    private fun WebView.trickProWeb() {
        this.apply {
            setLayerType(View.LAYER_TYPE_HARDWARE, null)
            this.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    trickPro.visibility = View.GONE
                }

                override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
                    when {
                        url.startsWith("mailto:") -> {
                            startActivity(Intent(Intent.ACTION_SENDTO, Uri.parse(url)))
                        }
                        url.startsWith("tel:") -> {
                            startActivity(Intent(Intent.ACTION_DIAL, Uri.parse(url)))
                        }
                        else -> {
                            view?.loadUrl(url)
                        }
                    }
                    return true
                }
            }
            this.webChromeClient = object : WebChromeClient() {

                override fun onShowFileChooser(
                    webView: WebView?,
                    filePathCallback: ValueCallback<Array<Uri?>>,
                    fileChooserParams: FileChooserParams?
                ): Boolean {
                    uriCallback = filePathCallback
                    superChooser()
                    return true
                }


                override fun onCloseWindow(window: WebView?) {
                    super.onCloseWindow(window)

                    trickWeb?.destroy()
                    trickCont.removeView(trickWeb)
                    trickView.visibility = View.VISIBLE
                }


                override fun onCreateWindow(
                    view: WebView?,
                    isDialog: Boolean,
                    isUserGesture: Boolean,
                    resultMsg: Message?
                ): Boolean {
                    val jokTP = resultMsg?.obj as WebView.WebViewTransport?
                    trickWeb = WebView(context).apply {
                        setLayerType(View.LAYER_TYPE_HARDWARE, null)
                        this.webViewClient = object : WebViewClient() {
                            override fun shouldOverrideUrlLoading(
                                view: WebView?,
                                url: String
                            ): Boolean {
                                view?.loadUrl(url)
                                return true
                            }
                        }
                        settings.apply {
                            javaScriptCanOpenWindowsAutomatically = true
                            setSupportMultipleWindows(true)
                            javaScriptEnabled = true
                            domStorageEnabled = true
                        }
                    }

                    trickCont.addView(trickWeb)
                    trickView.visibility = View.GONE

                    jokTP?.webView = trickWeb
                    resultMsg?.sendToTarget()
                    return true
                }
            }
        }
    }
}