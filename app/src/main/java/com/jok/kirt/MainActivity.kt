package com.jok.kirt

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var imageView: List<ImageView>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        imageView = listOf(
            puzzle1,
            puzzle2,
            puzzle3,
            puzzle4,
            puzzle5,
            puzzle6,
            puzzle7,
            puzzle8,
            puzzle9,
            puzzle10,
            puzzle11,
            puzzle12,
            puzzle13,
            puzzle14,
            puzzle15,
            puzzle16
        )

        changePiece()
        startGame()

    }

    private fun changePiece() {
        puzzle4.setOnClickListener {
            puzzle4.rotation += 90f
            if (puzzle4.rotation.toInt() % 360 == 0) {
                puzzle4.rotation = 0f
                isComplete()
            }
        }
        puzzle3.setOnClickListener {
            puzzle3.rotation += 90f
            if (puzzle3.rotation.toInt() % 360 == 0) {
                puzzle3.rotation = 0f
                isComplete()
            }
        }
        puzzle1.setOnClickListener {
            puzzle1.rotation += 90f
            if (puzzle1.rotation.toInt() % 360 == 0) {
                puzzle1.rotation = 0f
                isComplete()
            }
        }
        puzzle5.setOnClickListener {
            puzzle5.rotation += 90f
            if (puzzle5.rotation.toInt() % 360 == 0) {
                puzzle5.rotation = 0f
                isComplete()
            }
        }
        puzzle6.setOnClickListener {
            puzzle6.rotation += 90f
            if (puzzle6.rotation.toInt() % 360 == 0) {
                puzzle6.rotation = 0f
                isComplete()
            }
        }
        puzzle2.setOnClickListener {
            puzzle2.rotation += 90f
            if (puzzle2.rotation.toInt() % 360 == 0) {
                puzzle2.rotation = 0f
                isComplete()
            }
        }
        puzzle8.setOnClickListener {
            puzzle8.rotation += 90f
            if (puzzle8.rotation.toInt() % 360 == 0) {
                puzzle8.rotation = 0f
                isComplete()
            }
        }
        puzzle14.setOnClickListener {
            puzzle14.rotation += 90f
            if (puzzle14.rotation.toInt() % 360 == 0) {
                puzzle14.rotation = 0f
                isComplete()
            }
        }
        puzzle9.setOnClickListener {
            puzzle9.rotation += 90f
            if (puzzle9.rotation.toInt() % 360 == 0) {
                puzzle9.rotation = 0f
                isComplete()
            }
        }
        puzzle7.setOnClickListener {
            puzzle7.rotation += 90f
            if (puzzle7.rotation.toInt() % 360 == 0) {
                puzzle7.rotation = 0f
                isComplete()
            }
        }
        puzzle11.setOnClickListener {
            puzzle11.rotation += 90f

            if (puzzle11.rotation.toInt() % 360 == 0) {
                puzzle11.rotation = 0f
                isComplete()
            }
        }
        puzzle12.setOnClickListener {
            puzzle12.rotation += 90f

            if (puzzle12.rotation.toInt() % 360 == 0) {
                puzzle12.rotation = 0f
                isComplete()
            }
        }
        puzzle16.setOnClickListener {
            puzzle16.rotation += 90f
            if (puzzle16.rotation.toInt() % 360 == 0) {
                puzzle16.rotation = 0f
                isComplete()
            }
        }
        puzzle13.setOnClickListener {
            puzzle13.rotation += 90f
            if (puzzle13.rotation.toInt() % 360 == 0) {
                puzzle13.rotation = 0f
                isComplete()
            }
        }
        puzzle10.setOnClickListener {
            puzzle10.rotation += 90f
            if (puzzle10.rotation.toInt() % 360 == 0) {
                puzzle10.rotation = 0f
                isComplete()
            }
        }
        puzzle15.setOnClickListener {
            puzzle15.rotation += 90f
            if (puzzle15.rotation.toInt() % 360 == 0) {
                puzzle15.rotation = 0f
                isComplete()
            }
        }

    }

    private fun startGame() {
        resumeGame()
        pauseGame()
        privacyPolice()
        bt_startGame.setOnClickListener {
            ch_timer.base = SystemClock.elapsedRealtime()
            restartGame()
            cl_startGameLayout.visibility = View.GONE
            ch_timer.start()
            btn_menu.visibility = View.VISIBLE
            iv_congratulations.visibility = View.GONE
            ch_timer.visibility = View.VISIBLE
            btn_menu.isClickable = true
        }
    }

    private fun restartGame() {
        imageView.forEach { a ->
            val i = (0..3).random()
            a.rotation = i * 90f
        }
        imageView.forEach { a ->
            a.isEnabled = true
        }
        cl_startGameLayout.visibility = View.GONE
    }

    private fun privacyPolice() {
        btn_privacy.setOnClickListener {
            startActivity(Intent(this, PolicyActivity::class.java))
            finish()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun isComplete() {
        if (puzzle1.rotation == 0f &&
            puzzle2.rotation == 0f &&
            puzzle3.rotation == 0f &&
            puzzle4.rotation == 0f &&
            puzzle5.rotation == 0f &&
            puzzle6.rotation == 0f &&
            puzzle7.rotation == 0f &&
            puzzle8.rotation == 0f &&
            puzzle9.rotation == 0f &&
            puzzle10.rotation == 0f &&
            puzzle11.rotation == 0f &&
            puzzle12.rotation == 0f &&
            puzzle13.rotation == 0f &&
            puzzle14.rotation == 0f &&
            puzzle15.rotation == 0f &&
            puzzle16.rotation == 0f
        ) {
            imageView.forEach { view ->
                view.isEnabled = false
            }
            iv_congratulations.visibility = View.VISIBLE
            ch_timer.stop()
            cl_startGameLayout.visibility = View.VISIBLE
            btn_menu.visibility = View.GONE
        }
    }

    private fun resumeGame() {
        btn_resume.setOnClickListener {
            ch_timer.start()
            btn_resume.visibility = View.GONE
            cl_startGameLayout.visibility = View.GONE
            btn_menu.isClickable = true
        }
    }

    private fun pauseGame() {
        btn_menu.setOnClickListener {
            ch_timer.stop()
            btn_resume.visibility = View.VISIBLE
            cl_startGameLayout.visibility = View.VISIBLE
            btn_menu.isClickable = false
        }
    }

}