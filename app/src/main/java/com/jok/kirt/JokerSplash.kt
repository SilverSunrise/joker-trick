package com.jok.kirt

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.onesignal.OneSignal


class SplashScreen : AppCompatActivity() {

    private fun jokOffer() {
        startActivity(Intent(this, TrickActivity::class.java))
        finish()
    }

    private fun jokGameTrick() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        if (jokInfoCompleted()) {
            isClean()
        } else {
            jokData {
                isClean()
            }
        }
    }

    private fun isClean() {
        fun start() {
            if (trikInfo().allowed) {
                jokOffer()
            } else {
                jokGameTrick()
            }
        }

        if (!trikInfo().allowed) {
            OneSignal.setSubscription(false)
            Handler(Looper.getMainLooper()).postDelayed({
                start()
            }, 1000)
        } else {
            start()
        }
    }
}